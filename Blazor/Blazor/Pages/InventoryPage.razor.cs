﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Components;
using Microsoft.Extensions.Localization;

namespace Blazor.Pages
{
    public partial class InventoryPage
    {

        [Inject]
        public IStringLocalizer<InventoryPage> Localizer { get; set; }

    }
}
