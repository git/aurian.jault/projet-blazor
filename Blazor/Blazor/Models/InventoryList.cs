﻿using Microsoft.AspNetCore.Http.Features;

namespace Blazor.Models
{
    public partial class InventoryList
    {
        static public int size = 18;
        public List<InventoryItem> inventoryItems = new List<InventoryItem>(new InventoryItem[size]);

        public InventoryList() 
        {
            inventoryItems[0] = new InventoryItem();
        }
    }
}
