﻿using System.Collections;

namespace Blazor.Models
{
    public class InventoryItem
    {
        public Item item;
        int stack;
        public int Stack { get; set; }

        public InventoryItem()
        {
            item = new Item();
            Stack = 64;
        }
        public InventoryItem(Item item)
        {
            this.item = item;
            Stack = 1;
        }

        public InventoryItem(Item item, int stock)
        {
            this.item = item;
            Stack = stock;
        }
    }
}
