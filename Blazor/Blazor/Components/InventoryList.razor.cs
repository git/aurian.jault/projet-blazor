﻿using Blazor.Models;
using Blazor.Pages;
using Blazor.Services;
using Blazored.Modal;
using Blazored.Modal.Services;
using Blazorise.DataGrid;
using Microsoft.AspNetCore.Components;
using Microsoft.Extensions.Localization;

namespace Blazor.Components
{
        public partial class InventoryList
        {
            private List<Item> items;

            private int totalItem;

            [Inject]
            public IDataService DataService { get; set; }

            [Inject]
            public IWebHostEnvironment WebHostEnvironment { get; set;}

            [Inject]
            public NavigationManager NavigationManager { get; set; }

            [CascadingParameter]
            public IModalService Modal { get; set; }

            [Inject]
            public IStringLocalizer<InventoryList> Localizer { get; set; }

            [CascadingParameter]
            public Inventory Parent { get; set; }

        protected override async Task OnInitializedAsync()
        {
            items = await DataService.getAll();
            totalItem = await DataService.Count();
            await base.OnInitializedAsync();
        }
    }
}