﻿using Blazor.Models;
using Microsoft.AspNetCore.Components;

namespace Blazor.Components
{
    public partial class InventoryListImage
    {

        [Parameter]
        public Item Item { get; set; }

        [CascadingParameter]
        public Inventory Parent { get; set; }

        internal void OnDragEnter()
        {

            Parent.Actions.Add(new InventoryAction("On drag enter", Item.Id, Item));
            return;
        }

        internal void OnDragLeave()
        {
            Parent.Actions.Add(new InventoryAction("On drag leave", Item.Id, Item));
            return;
        }

        internal void OnDrop()
        {
            Parent.Actions.Add(new InventoryAction("On drop", Item.Id, Item));
            Parent.CurrentDragItem = null;
            return;
        }

        private void OnDragStart()
        {
            Parent.CurrentDragItem = new InventoryItem(Item);
            Parent.Actions.Add(new InventoryAction("On drag start", Item.Id, Item));
            return;
        }
    }
}
