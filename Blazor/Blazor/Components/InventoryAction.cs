﻿using Blazor.Models;

namespace Blazor.Components
{
    public class InventoryAction
    {
        public InventoryAction(String ac, int num,Item objet) {
            Action = ac;
            Index = num;
            Item = objet;
        }
        public string Action { get; set; }
        public int Index { get; set; }
        public Item Item { get; set; }
    }
}
